from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)
        

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}  # see ConferenceDetailEncoder class in events/api_views

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            # add its return value to d with key, "href"
            for property in self.properties: 
        #     * for each name in the properties list
                value = getattr(o, property)
        #         * get the value of that property from the model instance
        #           given just the property name
                if property in self.encoders:
                    encoder = self.encoders[property]  #  we're formatting conference lists/details to be JSO friendly, remember?
                    value = encoder.default(value)
                d[property] = value
        #         * put it into the dictionary with that property name as the key
            d.update(self.get_extra_data(o)) #  somehow handles edge cases
        #     * return the dictionary
            return d
        else:
            return super().default(o)  # From the documentation


    def get_extra_data(self, o):
        return {}
