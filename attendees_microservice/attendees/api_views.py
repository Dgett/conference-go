from django.http import JsonResponse
from .models import Attendee, ConferenceVO 
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(), 
    }




 # changed conference_id to conference_vo_id=None   
@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.
    
    attendees = [
            {
                "name": a.name,
                "href": a.get_api_url(),
            }
            for a in Attendee.objects.filter(conference=conference_id)
        ]
    return JsonResponse({"attendees": attendees})
    """
    # conference = conference_id instead of objects.all()   changed to _vo_id for microservices
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    
    try:
        # this line is added for VO
        conference_href = f'/api/conferences/{conference_vo_id}/'
        # id=conference_id changed to get(import_href=conference_href for VO)
        conference = ConferenceVO.objects.get(import_href=conference_href)
        content["conference"] = conference
        ## THIS CHANGES TO ConferenceVO from Conference
    except ConferenceVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )
    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
    {
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": attendee.conference.get_api_url(),
        }
    }
    )
    """
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
    # copied from create request example
        content = json.loads(request.body)
        # try:
        # new code
        #    if "state" in content:
        #        state = State.objects.get(abbreviation=content["state"])
        #        content["state"] = state
        #except State.DoesNotExist:
        #    return JsonResponse(
        #        {"message": "Invalid state abbreviation"},
        #        status=400,
        #    )

    # new code
        Attendee.objects.filter(id=id).update(**content)

    # copied from get detail
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
    
