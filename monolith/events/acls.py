from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

    # acl means anti corruption layer
def get_photo(city, state):
    # https://www.pexels.com/api/documentation/#photos-overview
    url = "http://api.pexels.com/v1/search"
    header = {"Authorization": PEXELS_API_KEY}
    payload = {"query": city + " " + state}
    #requests documentation
    jstring = requests.get(url, headers=header, params=payload)
    print(jstring)
    new_dict = json.loads(jstring.text)
    
    # how to know exactly which keys, values contain photos
    #rg = new_dict["photos"][0]["src"]
