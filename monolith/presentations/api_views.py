from django.http import JsonResponse
from .models import Presentation
from events.api_views import ConferenceListEncoder
from common.json import ModelEncoder
from events.models import Conference
import json
#from presentations.api_views import PresentationListEncoder
from django.views.decorators.http import require_http_methods

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title",]

    def _get_extra_data(self, o):
        return {"status": o.status.name}
    
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.
    
    presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]
    return JsonResponse({"presentations": presentations})
    
    presentations = Presentation.objects.all()
    return JsonResponse(
        {"presentations": presentations},
        encoder=PresentationListEncoder,
    )
    """
@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)

    # I guess every presentation has a conference ID
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
        )
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL
    """

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
    # copied from create
        content = json.loads(request.body)
        #try:
        # new code
        #    if "state" in content:
        #        state = State.objects.get(abbreviation=content["state"])
        #        content["state"] = state
        #except State.DoesNotExist:
        #    return JsonResponse(
        #        {"message": "Invalid state abbreviation"},
        #        status=400,
        #    )

    # new code
        Presentation.objects.filter(id=id).update(**content)

    # copied from get detail
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
       # "status",  not needed because get_extra_data method handles it.
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
        #  "status": Presentation()  see above OR I could put it here and build an encoder for it like I have with conferenceListEncoder
    }

    def _get_extra_data(self, o):
        return {"status": o.status.name}
"""
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
    {
        "presenter_name": presentation.presenter_name,
        "company_name": presentation.company_name, 
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
        "synopsis": presentation.synopsis,
        "created": presentation.created,
        "status": presentation.status.name,
        "conference": {
            "name": presentation.conference.name,
            "href": presentation.conference.get_api_url(),
        }
    }
    )
"""
